let data = require("./p2")

/*let data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]*/


//1. Find all people who are Agender
let agenderList = data.filter((each,index,arr)=> {
    return each.gender.includes("Agender")
})
console.log(agenderList)
console.log('\n')


//2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
let splittedIPAddresses = data.map((each,index,arr) =>{
    return each.ip_address.split(".")
})
console.log(splittedIPAddresses)
console.log('\n')


//3. Find the sum of all the second components of the ip addresses.

let components_2nd = splittedIPAddresses.map((each,index,arr) =>{
    return parseInt(each[1])
})

let sumOf2ndComponents = components_2nd.reduce((a,b) => a + b)
console.log(`Sum of all the 2nd components : ${sumOf2ndComponents}`)
console.log('\n')


//3. Find the sum of all the fourth components of the ip addresses.

let components_4th = splittedIPAddresses.map((each,index,arr) =>{
    return parseInt(each[3])
})

let sumOf4thComponents = components_4th.reduce((a,b) => a + b)
console.log(`Sum of all the 4th components : ${sumOf4thComponents}`)
console.log('\n')


//4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
let data_with_full_name = data.map((each,index,arr) => {
    let fullName = each.first_name + " " + each.last_name
    each["full_name"] = fullName 
    return each
})
console.log(data_with_full_name)
console.log('\n')

//5. Filter out all the .org emails
let org_emails_data = data.filter((each,index,arr) => {
    return (each.email.endsWith(".org"))
})
let org_emails = org_emails_data.map(each => {
    return each.email
})
console.log(org_emails)
console.log('\n')


//6. Calculate how many .org, .au, .com emails are there

let no_of_org_emails = org_emails.length
console.log(`no og .org emails : ${no_of_org_emails}`)
console.log('\n')

let au_emails_data = data.filter((each,index,arr) => {
    return (each.email.endsWith(".au"))
})
let au_emails = au_emails_data.map(each =>{
    return each.email
})
let no_of_au_emails = au_emails.length
console.log(`no og .au emails : ${no_of_au_emails}`)
console.log('\n')

let com_emails_data = data.filter((each,index,arr) => {
    return (each.email.endsWith(".com"))
})
let com_emails = com_emails_data.map(each =>{
    return each.email
})
let no_of_com_emails = com_emails.length
console.log(`no of .com emails : ${no_of_com_emails}`)
console.log('\n')


//7. Sort the data in descending order of first name

let sorted_data = data.sort((a,b) =>{
    if (a.first_name > b.first_name) return -1
    else if (a.first_name < b.first_name) return 1
    else if(a.first_name === b.first_name) return 0
})
console.log(sorted_data)